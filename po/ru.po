# Translation of gnome-games to Russian
# Copyright (C) 1999-2002,2003, 2004, 2005, 2010 Free Software Foundation, Inc.
#
# Sergey Panov <sipan@mit.edu>, 1999.
# Dmitry G. Mastrukov <dmitry@taurussoft.org>, 2002-2003.
# Vyacheslav Dikonov <linuxbox@degunino.net>, 2002.
# Michael Yakhontov <mvy@asplinux.ru>, 2003.
# Leonid Kanter <leon@asplinux.ru>, 2004, 2005.
# Dmitry Dzhus <dima@sphinx.net.ru>, 2008.
# Andrew Grigorev <andrew@ei-grad.ru>, 2009.
# Yuri Kozlov <yuray@komyakino.ru>, 2010.
# Valek Filippov <frob@df.ru> 2000-2002
# Nickolay V. Shmyrev <nshmyrev@yandex.ru>
# Yuri Myasoedov <ymyasoedov@yandex.ru>, 2012-2014.
# Ivan Komaritsyn <vantu5z@mail.ru>, 2015.
# Stas Solovey <whats_up@tut.by>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-games trunk\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/four-in-a-row/issues\n"
"POT-Creation-Date: 2020-05-30 11:03+0000\n"
"PO-Revision-Date: 2021-10-19 14:40+0300\n"
"Last-Translator: Alexey Rubtsov <rushills@gmail.com>\n"
"Language-Team: Русский <gnome-cyr@gnome.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 3.0\n"

#. Translators: when configuring a new game, on a thin window, header of the row for choosing the number of players
#: data/ui/fiar-screens.ui:40
msgid "Game type"
msgstr "Тип игры"

#. Translators: when configuring a new game, on a thin window, group "Game type", label of the button to choose to play first (with a mnemonic that appears pressing Alt)
#. Translators: when configuring a new game, row "Game start", label of the button to start (with a mnemonic that appears pressing Alt)
#: data/ui/fiar-screens.ui:61 data/ui/fiar-screens.ui:244
msgid "Play _first"
msgstr "Играть _первым"

#. Translators: when configuring a new game, on a thin window, group "Game type", label of the button to choose to play second (with a mnemonic that appears pressing Alt)
#. Translators: when configuring a new game, row "Game start", label of the button to play after the computer (with a mnemonic that appears pressing Alt)
#: data/ui/fiar-screens.ui:73 data/ui/fiar-screens.ui:256
msgid "Play _second"
msgstr "Играть _вторым"

#. Translators: when configuring a new game, on a thin window, group "Game type", label of the button to choose a two-players game (with a mnemonic that appears pressing Alt)
#: data/ui/fiar-screens.ui:85
msgid "_Two players"
msgstr "Два игрока"

#. Translators: when configuring a new game, header of the row for choosing the number of players
#: data/ui/fiar-screens.ui:109
msgid "Players"
msgstr "Игроки"

#. Translators: when configuring a new game, row "Players", label of the button to choose a one-player game (with a mnemonic that appears pressing Alt)
#: data/ui/fiar-screens.ui:126
msgid "_One"
msgstr "_Один"

#. Translators: when configuring a new game, row "Players", label of the button to choose a one-player game (with a mnemonic that appears pressing Alt)
#: data/ui/fiar-screens.ui:138
msgid "_Two"
msgstr "_Два"

#. Translators: when configuring a new game, header of the row for choosing the level of the artificial intelligence
#: data/ui/fiar-screens.ui:162
msgid "Difficulty"
msgstr "Сложность"

#. Translators: when configuring a new game, row "Difficulty", label of the button to choose an easy-level computer adversary (with a mnemonic that appears pressing Alt)
#: data/ui/fiar-screens.ui:179
msgid "_Easy"
msgstr "_Низкая"

#. Translators: when configuring a new game, row "Difficulty", label of the button to choose a medium-level computer adversary (with a mnemonic that appears pressing Alt)
#: data/ui/fiar-screens.ui:191
msgid "_Medium"
msgstr "_Средняя"

#. Translators: when configuring a new game, row "Difficulty", label of the button to choose a hard-level computer adversary (with a mnemonic that appears pressing Alt)
#: data/ui/fiar-screens.ui:203
msgid "_Hard"
msgstr "_Высокая"

#. Translators: when configuring a new game, header of the row for choosing whether to start or not
#: data/ui/fiar-screens.ui:227
msgid "Game start"
msgstr "Начало игры"

#. Translators: during a game, label of the New Game button (with a mnemonic that appears pressing Alt)
#: data/ui/four-in-a-row.ui:31
msgid "_New Game"
msgstr "_Новая игра"

#. Translators: during a game, tooltip text of the New Game button
#: data/ui/four-in-a-row.ui:36
msgid "Start a new game"
msgstr "Начать новую игру"

#. Translators: when configuring a new game, if the user has a started game, tooltip text of the Go back button
#: data/ui/four-in-a-row.ui:49
msgid "Go back to the current game"
msgstr "Вернуться к текущей игре"

#. Translators: section of the Keyboard Shortcuts window; contains shortcuts available during a game: "New game", "Undo last move"...
#: data/ui/help-overlay.ui:31
msgctxt "shortcut window"
msgid "During a game"
msgstr "Во время игры"

#. Translators: in the Keyboard Shortcuts window, section "During a game"
#: data/ui/help-overlay.ui:36
msgctxt "shortcut window"
msgid "New game"
msgstr "Новая игра"

#. Translators: in the Keyboard Shortcuts window, section "During a game"
#: data/ui/help-overlay.ui:44
msgctxt "shortcut window"
msgid "Undo last move"
msgstr "Отменить последний ход"

#. Translators: in the Keyboard Shortcuts window, section "During a game"
#: data/ui/help-overlay.ui:52
msgctxt "shortcut window"
msgid "Show a hint"
msgstr "Показать подсказку"

#. Translators: in the Keyboard Shortcuts window, section "During a game"
#: data/ui/help-overlay.ui:60
msgctxt "shortcut window"
msgid "Toggle game menu"
msgstr "Переключить меню игры"

#. Translators: section of the Keyboard Shortcuts window; contains shortucts for playing: Left/Right/Down
#: data/ui/help-overlay.ui:70
msgctxt "shortcut window"
msgid "Play with keyboard"
msgstr "Играть с клавиатуры"

#. Translators: in the Keyboard Shortcuts window, section "Play with keyboard"; using arrows is the usual way select where to play
#: data/ui/help-overlay.ui:75
msgctxt "shortcut window"
msgid "Select where to play"
msgstr "Выберите, где играть"

#. Translators: in the Keyboard Shortcuts window, section "Play with keyboard"; shortcut for dropping the tile
#: data/ui/help-overlay.ui:83
msgctxt "shortcut window"
msgid "Play in selected column"
msgstr "Воспроизведение в выбранной колонке"

#. Translators: in the Keyboard Shortcuts window, section "Play with keyboard"; hitting number plays directly to the nth column
#: data/ui/help-overlay.ui:91
msgctxt "shortcut window"
msgid "Play in given column"
msgstr "Играть в данной колонке"

#. Translators: section of the Keyboard Shortcuts window; contains shortcuts available when configuring a new game
#: data/ui/help-overlay.ui:101
msgctxt "shortcut window"
msgid "During game selection"
msgstr "Во время выбора игры"

#. Translators: in the Keyboard Shortcuts window, section "During game selection"; how to launch a new game
#: data/ui/help-overlay.ui:106
msgctxt "shortcut window"
msgid "Start new game"
msgstr "Начать новую игру"

#. Translators: in the Keyboard Shortcuts window, section "During game selection"; how to go back to a running game, if any
#: data/ui/help-overlay.ui:114
msgctxt "shortcut window"
msgid "Go back"
msgstr "Назад"

#. Translators: section of the Keyboard Shortcuts window; contains "Help", "About", "Quit"...
#: data/ui/help-overlay.ui:124
msgctxt "shortcut window"
msgid "Generic"
msgstr "Общие"

#. Translators: in the Keyboard Shortcuts window, section "Generic"
#: data/ui/help-overlay.ui:129
msgctxt "shortcut window"
msgid "Toggle main menu"
msgstr "Переключить главное меню"

#. Translators: in the Keyboard Shortcuts window, section "Generic"
#: data/ui/help-overlay.ui:137
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Сочетания клавиш"

#. Translators: in the Keyboard Shortcuts window, section "Generic"
#: data/ui/help-overlay.ui:145
msgctxt "shortcut window"
msgid "Help"
msgstr "Справка"

#. Translators: in the Keyboard Shortcuts window, section "Generic"
#: data/ui/help-overlay.ui:153
msgctxt "shortcut window"
msgid "About"
msgstr "О приложении"

#. Translators: in the Keyboard Shortcuts window, section "Generic"
#: data/ui/help-overlay.ui:161
msgctxt "shortcut window"
msgid "Quit"
msgstr "Выход"

#. Translators: label of the game status button (in the headerbar, next to the hamburger button); please keep the string as small as possible (3~5 characters)
#: data/ui/history-button.ui:41
msgid "End!"
msgstr "Конец!"

#. Translators: application name, as used in the window manager, the window title, the about dialog...
#: data/org.gnome.Four-in-a-row.appdata.xml.in:7
#: data/org.gnome.Four-in-a-row.desktop.in:5 src/four-in-a-row.vala:29
msgid "Four-in-a-row"
msgstr "Четыре в ряд"

#: data/org.gnome.Four-in-a-row.appdata.xml.in:8
#: data/org.gnome.Four-in-a-row.desktop.in:6
msgid "Make lines of the same color to win"
msgstr "Для победы выстраивайте ряды шариков одинакового цвета"

#: data/org.gnome.Four-in-a-row.appdata.xml.in:10
msgid ""
"A family classic, the objective of Four-in-a-row is to build a line of four "
"of your marbles while trying to stop your opponent (human or computer) from "
"building a line of his or her own. A line can be horizontal, vertical or "
"diagonal. The first player to connect four in a row is the winner!"
msgstr ""
"Цель игры \"Четыре в ряд\" - выстроить линию из четырех своих шариков, "
"стараясь помешать противнику (человеку или компьютеру) выстроить свою линию. "
"Линия может быть горизонтальной, вертикальной или диагональной. Победителем "
"становится игрок, первым выстроивший четыре шарика в ряд!"

#: data/org.gnome.Four-in-a-row.appdata.xml.in:16
msgid ""
"Four-in-a-row features multiple difficulty levels. If you’re having trouble, "
"you can always ask for a hint."
msgstr ""
"В игре «Четыре в ряд» есть несколько уровней сложности. Если у вас возникнут "
"трудности, вы всегда можете попросить подсказку."

#: data/org.gnome.Four-in-a-row.appdata.xml.in:37
msgid "The GNOME Project"
msgstr "Проект GNOME"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Four-in-a-row.desktop.in:8
msgid "game;strategy;logic;"
msgstr "игра;стратегия;логическая;"

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/first-player'
#: data/org.gnome.Four-in-a-row.gschema.xml:11
msgid "Who starts"
msgstr "Кто начинает"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/first-player'
#: data/org.gnome.Four-in-a-row.gschema.xml:13
msgid ""
"Specifies who will start the next one-player game. Ignored for two-player "
"games."
msgstr ""
"Указывает, кто начнет следующую игру для одного игрока. Игнорируется для игр "
"с двумя игроками."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/num-players'
#: data/org.gnome.Four-in-a-row.gschema.xml:19
msgid "Number of players"
msgstr "Количество участников"

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/opponent'
#: data/org.gnome.Four-in-a-row.gschema.xml:26
msgid "Opponent"
msgstr "Соперник"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/opponent'
#: data/org.gnome.Four-in-a-row.gschema.xml:28
msgid "From 1, the easiest, to 3, the hardest. Ignored for two-player games."
msgstr ""
"От 1, самого легкого, до 3, самого трудного. Игнорируется для игр с двумя "
"игроками."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/theme-id'
#: data/org.gnome.Four-in-a-row.gschema.xml:34
msgid "Theme ID"
msgstr "Идентификатор темы"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/theme-id'
#: data/org.gnome.Four-in-a-row.gschema.xml:36
msgid "A number specifying the preferred theme."
msgstr "Число, указывающее выбранную тему."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/sound'
#: data/org.gnome.Four-in-a-row.gschema.xml:41
msgid "Sound"
msgstr "Звук"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/sound'
#: data/org.gnome.Four-in-a-row.gschema.xml:43
msgid "Whether or not to play event sounds."
msgstr "Воспроизводить ли звуковые события."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/key-left'
#: data/org.gnome.Four-in-a-row.gschema.xml:48
msgid "Move left"
msgstr "Влево"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/key-left'
#: data/org.gnome.Four-in-a-row.gschema.xml:50
msgid "Key press to move left."
msgstr "Клавиша для перемещения влево."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/key-right'
#: data/org.gnome.Four-in-a-row.gschema.xml:55
msgid "Move right"
msgstr "Вправо"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/key-right'
#: data/org.gnome.Four-in-a-row.gschema.xml:57
msgid "Key press to move right."
msgstr "Клавиша для перемещения вправо."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/key-drop'
#: data/org.gnome.Four-in-a-row.gschema.xml:62
msgid "Drop marble"
msgstr "Бросить шарик"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/key-drop'
#: data/org.gnome.Four-in-a-row.gschema.xml:64
msgid "Key press to drop a marble."
msgstr "Клавиша для сброса шарика."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/window-width'
#: data/org.gnome.Four-in-a-row.gschema.xml:72
msgid "The width of the window"
msgstr "Ширина окна"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/window-width'
#: data/org.gnome.Four-in-a-row.gschema.xml:74
msgid "The width of the main window in pixels."
msgstr "Ширина главного окна в пикселях."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/window-height'
#: data/org.gnome.Four-in-a-row.gschema.xml:79
msgid "The height of the window"
msgstr "Высота окна"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/window-height'
#: data/org.gnome.Four-in-a-row.gschema.xml:81
msgid "The height of the main window in pixels."
msgstr "Высота главного окна в пикселях."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/window-is-maximized'
#: data/org.gnome.Four-in-a-row.gschema.xml:86
msgid "A flag to enable maximized mode"
msgstr "Флаг для включения режима максимизации"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Four-in-a-row/window-is-maximized'
#: data/org.gnome.Four-in-a-row.gschema.xml:88
msgid "If “true”, the main window starts in maximized mode."
msgstr "Если \"true\", главное окно запускается в режиме максимизации."

#. Translators: command-line option description, see 'four-in-a-row --help'
#: src/four-in-a-row.vala:95
msgid "Set the level of the computer’s AI"
msgstr "Установите уровень искусственного интеллекта компьютера"

#. Translators: in the command-line options description, text to indicate the user should specify a level, see 'four-in-a-row --help'
#: src/four-in-a-row.vala:98
msgid "LEVEL"
msgstr "LEVEL"

#. Translators: command-line option description, see 'four-in-a-row --help'
#: src/four-in-a-row.vala:101
msgid "Turn off the sound"
msgstr "Выключите звук"

#. Translators: command-line option description, see 'four-in-a-row --help'
#: src/four-in-a-row.vala:104
msgid "Size of the board"
msgstr "Размер доски"

#. Translators: in the command-line options description, text to indicate the user should specify a size, see 'four-in-a-row --help'
#: src/four-in-a-row.vala:107
msgid "SIZE"
msgstr "SIZE"

#. Translators: command-line option description, see 'four-in-a-row --help'
#: src/four-in-a-row.vala:110
msgid "Length of a winning line"
msgstr "Длина выигрышной линии"

#. Translators: in the command-line options description, text to indicate the user should specify the line length, see 'four-in-a-row --help'
#: src/four-in-a-row.vala:113
msgid "TARGET"
msgstr "TARGET"

#. Translators: command-line option description, see 'four-in-a-row --help'
#: src/four-in-a-row.vala:116
msgid "Turn on the sound"
msgstr "Включите звук"

#. Translators: command-line option description, see 'four-in-a-row --help'
#: src/four-in-a-row.vala:119
msgid "Print release version and exit"
msgstr "Печать версии релиза и выход"

#. Translators: command-line error message, displayed for an incorrect game size request; try 'four-in-a-row -s 2'
#: src/four-in-a-row.vala:167
msgid "Size must be at least 4."
msgstr "Размер должен быть не менее 4."

#. Translators: command-line error message, displayed for an incorrect game size request; try 'four-in-a-row -s 17'
#: src/four-in-a-row.vala:173
msgid "Size must not be more than 16."
msgstr "Размер не должен быть больше 16."

#. Translators: command-line error message, displayed for an incorrect line length request; try 'four-in-a-row -t 2'
#: src/four-in-a-row.vala:180
msgid "Lines must be at least 3 tiles."
msgstr "Линии должны быть не менее 3 плиток."

#. Translators: command-line error message, displayed for an incorrect line length request; try 'four-in-a-row -t 8'
#: src/four-in-a-row.vala:186
msgid "Lines cannot be longer than board height or width."
msgstr "Строки не могут быть длиннее высоты или ширины доски."

#. Translators: command-line error message, displayed for an incorrect level request; try 'four-in-a-row -l 5'
#: src/four-in-a-row.vala:229
msgid "Level should be 1 (easy), 2 (medium) or 3 (hard). Settings unchanged."
msgstr ""
"Уровень должен быть 1 (легкий), 2 (средний) или 3 (трудный). Настройки не "
"изменены."

#. Translators: hamburger menu entry; "Appearance" submenu (with a mnemonic that appears pressing Alt)
#: src/four-in-a-row.vala:267
msgid "A_ppearance"
msgstr "_Вид"

#. Translators: hamburger menu entry with checkbox, for activating or disactivating sound (with a mnemonic that appears pressing Alt)
#: src/four-in-a-row.vala:271
msgid "_Sound"
msgstr "_Звук"

#. Translators: hamburger menu entry; opens the Keyboard Shortcuts dialog (with a mnemonic that appears pressing Alt)
#: src/four-in-a-row.vala:277
msgid "_Keyboard Shortcuts"
msgstr "_Комбинации клавиш"

#. Translators: hamburger menu entry; opens the application help (with a mnemonic that appears pressing Alt)
#: src/four-in-a-row.vala:281
msgid "_Help"
msgstr "Под_сказка"

#. Translators: hamburger menu entry; opens the About dialog (with a mnemonic that appears pressing Alt)
#: src/four-in-a-row.vala:285
msgid "_About Four-in-a-row"
msgstr "_Об игре «Четыре в ряд»"

#. Translators: text displayed on game end in the headerbar/actionbar, if the game is a tie
#: src/four-in-a-row.vala:567
msgid "It’s a draw!"
msgstr "Ничья!"

#. Translators: text displayed on a one-player game end in the headerbar/actionbar, if the human player won
#: src/four-in-a-row.vala:581
msgid "You win!"
msgstr "Вы победили!"

#. Translators: text displayed on a one-player game end in the headerbar/actionbar, if the computer player won
#: src/four-in-a-row.vala:584
msgid "I win!"
msgstr "Я победил!"

#. Translators: text displayed during a one-player game in the headerbar/actionbar, if it is the human player's turn
#: src/four-in-a-row.vala:593
msgid "Your Turn"
msgstr "Ваш ход"

#. Translators: text displayed during a one-player game in the headerbar/actionbar, if it is the computer player's turn
#. Translators: text *briefly* displayed in the headerbar/actionbar, when a hint is requested
#: src/four-in-a-row.vala:600 src/four-in-a-row.vala:909
msgid "I’m Thinking…"
msgstr "Я думаю…"

#. blink n times
#. Translators: text displayed in the headerbar/actionbar, when a hint is requested; the %d is replaced by the number of the suggested column
#: src/four-in-a-row.vala:925
#, c-format
msgid "Hint: Column %d"
msgstr "Подсказка: столбец %d"

#. Translators: in the About dialog, name of an author of the game
#: src/four-in-a-row.vala:1122
msgid "Tim Musson"
msgstr "Тим Муссон (Tim Musson)"

#. Translators: in the About dialog, name of an author of the game
#: src/four-in-a-row.vala:1126
msgid "David Neary"
msgstr "Дэвид Нери (David Neary)"

#. Translators: in the About dialog, name of an author of the game
#: src/four-in-a-row.vala:1130
msgid "Nikhar Agrawal"
msgstr "Нихар Агравал (Nikhar Agrawal)"

#. Translators: in the About dialog, name of an author of the game
#: src/four-in-a-row.vala:1134
msgid "Jacob Humphrey"
msgstr "Джейкоб Хамфри (Jacob Humphrey)"

#. Translators: in the About dialog, name of an author of the game
#: src/four-in-a-row.vala:1138
msgid "Arnaud Bonatti"
msgstr "Арно Бонатти (Arnaud Bonatti)"

#. Translators: in the About dialog, name of a theme designer
#: src/four-in-a-row.vala:1143
msgid "Alan Horkan"
msgstr "Алан Хоркан (Alan Horkan)"

#. Translators: in the About dialog, name of a theme designer
#: src/four-in-a-row.vala:1147
msgid "Anatol Drlicek"
msgstr "Анатоль Дрличек (Anatol Drlicek)"

#. Translators: in the About dialog, indication about some themes origin
#: src/four-in-a-row.vala:1151
msgid "Based on the Faenza icon theme by Matthieu James"
msgstr ""
"На основе темы значков Faenza, созданной Матье Джеймсом (Matthieu James)"

#. Translators: in the About dialog, name of a documenter
#: src/four-in-a-row.vala:1155
msgid "Timothy Musson"
msgstr "Тимоти Муссон (Timothy Musson)"

#. Translators: text crediting a maintainer, in the about dialog text
#: src/four-in-a-row.vala:1159
msgid "Copyright © 1999-2008 – Tim Musson and David Neary"
msgstr "Авторское право © 1999-2008 - Тим Муссон и Дэвид Нери"

#. Translators: text crediting a maintainer, in the about dialog text
#: src/four-in-a-row.vala:1163
msgid "Copyright © 2014 – Michael Catanzaro"
msgstr "Авторское право © 2014 - Майкл Катанзаро"

#. Translators: text crediting a maintainer, in the about dialog text
#: src/four-in-a-row.vala:1167
msgid "Copyright © 2018 – Jacob Humphrey"
msgstr "Copyright © 2018 - Джейкоб Хамфри"

#. Translators: text crediting a maintainer, in the about dialog text; the %u are replaced with the years of start and end
#: src/four-in-a-row.vala:1171
#, c-format
msgid "Copyright © %u-%u – Arnaud Bonatti"
msgstr "Copyright © %u-%u - Арно Бонатти"

#. Translators: about dialog text, introducing the game
#: src/four-in-a-row.vala:1179
msgid "Connect four in a row to win"
msgstr "Соедините четыре в ряд, чтобы выиграть"

#. Translators: about dialog text; this string should be replaced by a text crediting yourselves and your translation team, or should be left empty. Do not translate literally!
#: src/four-in-a-row.vala:1184
msgid "translator-credits"
msgstr ""
"Yuri Myasoedov <ymyasoedov@yandex.ru>, 2012-2014.\n"
"Stas Solovey <whats_up@tut.by>, 2016-2018.\n"
"Alexey Rubtsov <rushills@gmail.com>, 2021"

#. Translators: during a game, entry in the game menu, for undoing the last move
#: src/four-in-a-row.vala:1285
msgid "_Undo last move"
msgstr "Отменить последнее движение"

#. Translators: during a game, entry in the game menu, for suggesting where to play
#: src/four-in-a-row.vala:1289
msgid "_Hint"
msgstr "_Подсказка"

#: src/four-in-a-row.vala:1301
msgid "Next _Round"
msgstr "Следующий _раунд"

#: src/four-in-a-row.vala:1303
msgid "_Give Up"
msgstr "_Сдаться"

#. Translators: hamburger menu entry; opens the Scores dialog (with a mnemonic that appears pressing Alt)
#: src/four-in-a-row.vala:1308
msgid "_Scores"
msgstr "_Результаты"

#. Translators: when configuring a new game, label of the blue Start button (with a mnemonic that appears pressing Alt)
#: src/game-window.vala:113
msgid "_Start Game"
msgstr "_Начать игру"

#. Translators: title of the Scores dialog; plural noun
#: src/scorebox.vala:37
msgid "Scores"
msgstr "Результаты"

#. Translators: in the Scores dialog, label of the line where is indicated the number of tie games
#: src/scorebox.vala:72
msgid "Drawn:"
msgstr "Ничья:"

#. Translators: in the Scores dialog, label of the line where is indicated the number of games won by the human player
#: src/scorebox.vala:93 src/scorebox.vala:107
msgid "You:"
msgstr "Вы:"

#. Translators: in the Scores dialog, label of the line where is indicated the number of games won by the computer player
#: src/scorebox.vala:96 src/scorebox.vala:104
msgid "Me:"
msgstr "Я:"

#. Translators: name of a black-on-white theme, for helping people with visual misabilities
#: src/theme.vala:170
msgid "High Contrast"
msgstr "Высококонтрастная"

#: src/theme.vala:174 src/theme.vala:185
msgid "Circle"
msgstr "Нолики"

#: src/theme.vala:174 src/theme.vala:185
msgid "Cross"
msgstr "Крестики"

#: src/theme.vala:175 src/theme.vala:186
msgid "Circle:"
msgstr "Нолики:"

#: src/theme.vala:175 src/theme.vala:186
msgid "Cross:"
msgstr "Крестики:"

#: src/theme.vala:176 src/theme.vala:187
msgid "Circle wins!"
msgstr "Нолики победили!"

#: src/theme.vala:176 src/theme.vala:187
msgid "Cross wins!"
msgstr "Крестики победили!"

#: src/theme.vala:177 src/theme.vala:188
msgid "Circle’s turn"
msgstr "Крестики ходят"

#: src/theme.vala:177 src/theme.vala:188
msgid "Cross’s turn"
msgstr "Нолики ходят"

#. Translators: name of a white-on-black theme, for helping people with visual misabilities
#: src/theme.vala:181
msgid "High Contrast Inverse"
msgstr "Высококонтрастная негативная"

#. Translators: name of a red-versus-green theme
#: src/theme.vala:192
msgid "Red and Green Marbles"
msgstr "Красные и зелёные шарики"

#: src/theme.vala:196 src/theme.vala:207 src/theme.vala:218
msgid "Red"
msgstr "Красный"

#: src/theme.vala:196 src/theme.vala:218
msgid "Green"
msgstr "Зелёный"

#: src/theme.vala:197 src/theme.vala:208 src/theme.vala:219
msgid "Red:"
msgstr "Красные:"

#: src/theme.vala:197 src/theme.vala:219
msgid "Green:"
msgstr "Зеленые:"

#: src/theme.vala:198 src/theme.vala:209 src/theme.vala:220
msgid "Red wins!"
msgstr "Красные победили!"

#: src/theme.vala:198 src/theme.vala:220
msgid "Green wins!"
msgstr "Зелёные победили!"

#: src/theme.vala:199 src/theme.vala:210 src/theme.vala:221
msgid "Red’s turn"
msgstr "Красные ходят"

#: src/theme.vala:199 src/theme.vala:221
msgid "Green’s turn"
msgstr "Зелёные ходят"

#. Translators: name of a blue-versus-red theme
#: src/theme.vala:203
msgid "Blue and Red Marbles"
msgstr "Синие и красные шарики"

#: src/theme.vala:207
msgid "Blue"
msgstr "Синий"

#: src/theme.vala:208
msgid "Blue:"
msgstr "Синие:"

#: src/theme.vala:209
msgid "Blue wins!"
msgstr "Синие победили!"

#: src/theme.vala:210
msgid "Blue’s turn"
msgstr "Синие ходят"

#. Translators: name of a red-versus-green theme with drawing on the tiles
#: src/theme.vala:214
msgid "Stars and Rings"
msgstr "Звёзды и кольца"

#~ msgid "four-in-a-row"
#~ msgstr "four-in-a-row"

#~ msgid "Receive a hint for your next move"
#~ msgstr "Получить подсказку для следующего хода"

#~ msgid ""
#~ "Zero is human; one through three correspond to the level of the computer "
#~ "player."
#~ msgstr ""
#~ "Ноль обозначает игрока-человека, а значение от единицы до тройки "
#~ "обозначает уровень мастерства компьютерного игрока."

#~ msgid "This key is already in use."
#~ msgstr "Данная клавиша уже используется."

#~ msgid "Unknown Command"
#~ msgstr "Неизвестная команда"

#~ msgid ""
#~ "Unable to load image:\n"
#~ "%s"
#~ msgstr ""
#~ "Не удалось загрузить изображение:\n"
#~ "%s"

#~ msgid "_Preferences"
#~ msgstr "_Параметры"

#~ msgid "Preferences"
#~ msgstr "Параметры"

#~ msgid "Opponent:"
#~ msgstr "Соперник:"

#~ msgid "Human"
#~ msgstr "Человек"

#~ msgid "Level one"
#~ msgstr "Первый уровень"

#~ msgid "Level two"
#~ msgstr "Второй уровень"

#~ msgid "Level three"
#~ msgstr "Третий уровень"

#~ msgid "_Theme:"
#~ msgstr "_Тема:"

#~ msgid "E_nable sounds"
#~ msgstr "Включить _звуки"
